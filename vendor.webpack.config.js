let path = require("path");
let webpack = require("webpack");
let webpackMerge = require("webpack-merge");

const root = path.resolve(__dirname);
const dist = path.join(root, "dist");

// https://robertknight.github.io/posts/webpack-dll-plugins/
module.exports = {
    entry: {
        polyfills: [
            path.join(root, "src", "polyfills.ts"),
        ],
        vendor: [
            path.join(root, "src", "vendor.ts"),
        ],
    },
    output: {
        filename: '[name].dll.js',
        library: '[name]_lib',
        path: dist,
    },
    plugins: [
        new webpack.DllPlugin({
            path: path.join(dist, '[name]-manifest.json'),
            name: '[name]_lib',
        }),
        // https://github.com/AngularClass/angular2-webpack-starter/issues/993#issuecomment-283423040
        new webpack.ContextReplacementPlugin(
            /angular(\\|\/)core(\\|\/)@angular/,
            path.join(root, "lib"), // location of your src
            {
                // your Angular Async Route paths relative to this root directory
            }
        ),
    ]
};
