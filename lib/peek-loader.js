const loaderUtils = require('loader-utils');

module.exports = function(content) {
    const callback = this.async();
    const options = loaderUtils.getOptions(this);
    options.stage || '';
    console.log("peek%s: %s", 
        options.stage ? ` ${options.stage}` : '',
        JSON.stringify(content, null, 2))
    callback(null, content);
};